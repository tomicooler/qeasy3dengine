TEMPLATE = subdirs

SUBDIRS += \
    QEasy3DEngine \
    Demo

CONFIG += ordered


## UWA (don't remove it)##
PREFIX = /media/Cuccos/uwa/install/QEasy3DEngine                   ## For QtCreator only
INCDIR = $$PREFIX/include
LIBDIR = $$PREFIX/lib
BINDIR = $$PREFIX/bin

LIBS += -L$$LIBDIR
INCLUDEPATH += $$INCDIR

# Useful commands:
#
# target.path = $$BINDIR
# INSTALLS += target
#
# headerinstall.files = $$HEADERS
# headerinstall.path = $$INCDIR
# INSTALLS += headerinstall
#
# LIBS += -lxy

