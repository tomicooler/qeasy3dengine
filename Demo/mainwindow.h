#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDesktopWidget>
#include <QApplication>

#include <QDebug>

#include "QEasy3DEngine/view.h"
#include "QEasy3DEngine/engine.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow, public QEasy3DEngine::CollisionListener
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  void keyPressEvent(QKeyEvent *event);

protected:
  void collisionHappend(QEasy3DEngine::Object *a, QEasy3DEngine::Object *b);

private:
  Ui::MainWindow *ui;

  QEasy3DEngine::View view3D;

  QEasy3DEngine::Object *groundObject;
  QEasy3DEngine::Object *adidasBall;

};

namespace
{

  /**
  * Enum type for position.
  */
  enum WHERE
  {
    TOPLEFT = 0,
    TOPRIGHT,
    CENTER,
    BOTTOMLEFT,
    BOTTOMRIGHT
  };

  /**
  * Move a QWidget to the screen specific position.
  *
  * @param widget
  * @param where
  */
  void
  moveWidgetOnScreen(QWidget *widget, WHERE where = CENTER)
  {

    QRect rect = QApplication::desktop()->availableGeometry();
    switch (where)
      {
      case TOPLEFT:
        widget->move(0, 0);
        break;
      case TOPRIGHT:
        widget->move(rect.width() - widget->width(), 0);
        break;
      case BOTTOMLEFT:
        widget->move(0, rect.height() - widget->height());
        break;
      case BOTTOMRIGHT:
        widget->move(rect.width() - widget->width(), rect.height() - widget->height());
        break;
      default: // center
        widget->move(rect.center() - widget->rect().center());
        break;
      }
  }

}

#endif // MAINWINDOW_H
