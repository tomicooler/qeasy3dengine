#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QKeyEvent>

#include <QEasy3DEngine/windowwidget.h>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  WindowWidget *w = new WindowWidget;
  w->setHostedWindow(&view3D);
  setCentralWidget(w);
  //view3D.show();
  //setCentralWidget(&view3D);

  view3D.camera()->setEye(QVector3D(150, 20, 0));

  {
    QEasy3DEngine::Shape *shape = new QEasy3DEngine::SphereShape("sphere-radius-3", 6);
    QEasy3DEngine::Engine::instance()->addShape(shape);
  }

  {
    QEasy3DEngine::Shape *shape = new QEasy3DEngine::SphereShape("sky", 800);
    QEasy3DEngine::Engine::instance()->addShape(shape);
  }

  {
    QEasy3DEngine::Shape *shape = new QEasy3DEngine::StaticPlaneShape("ground", 1000, 0.5, 1000);
    QEasy3DEngine::Engine::instance()->addShape(shape);
  }

  {
    QEasy3DEngine::Shape *shape = new QEasy3DEngine::BoxShape("box-a-3-b-3-c-3", 6, 6, 6);
    QEasy3DEngine::Engine::instance()->addShape(shape);
  }

//  {
//    Shape *shape = new AbstractShape("gate", qApp->applicationDirPath() + "/models/gate.obj");
//    Engine3D::instance()->addShape(shape);
//  }

  {
    QEasy3DEngine::Object *object = new QEasy3DEngine::Object;
    object->setDefaultMotionState(QQuaternion(1, 0, 0, 0), QVector3D(0, 0, 0));
    object->setShape(QEasy3DEngine::Engine::instance()->getShape("sky"));
    object->setMass(0);
    object->setTexture2D(":textures/sky.jpg");
    object->dontAddToDynmicsWorld();
    object->finalizeRigidBody();

    QEasy3DEngine::Engine::instance()->addObject(object);
  }

  {
    groundObject = new QEasy3DEngine::Object;
    groundObject->setDefaultMotionState(QQuaternion(1, 0, 0, 0), QVector3D(0, -1, 0));
    groundObject->setShape(QEasy3DEngine::Engine::instance()->getShape("ground"));
    groundObject->setRestitution(0.4f);
    groundObject->setFriction(2.0f);
    groundObject->setTexture2D(":textures/grass2.jpg");
    groundObject->finalizeRigidBody();

    QEasy3DEngine::Engine::instance()->addObject(groundObject);
  }

  {
    QEasy3DEngine::Object *object = new QEasy3DEngine::Object;
    object->setColor(Qt::red);
    object->setDefaultMotionState(QQuaternion(1, 0, 0, 0), QVector3D(0, 50, 0));
    object->setShape(QEasy3DEngine::Engine::instance()->getShape("sphere-radius-3"));
    object->setMass(1);
    object->setFriction(0.4);
    object->setRestitution(0.9);
    object->finalizeRigidBody();

    QEasy3DEngine::Engine::instance()->addObject(object);
  }

  {
    adidasBall = new QEasy3DEngine::Object;
    adidasBall->setTexture2D(":textures/ball.jpg");
    adidasBall->setDefaultMotionState(QQuaternion(1, 0, 0, 0), QVector3D(4, 40, 0));
    adidasBall->setShape(QEasy3DEngine::Engine::instance()->getShape("sphere-radius-3"));
    adidasBall->setMass(0.4);
    adidasBall->setFriction(0.4);
    adidasBall->setRestitution(0.9);
    adidasBall->disableDeactivation();
    adidasBall->finalizeRigidBody();

    QEasy3DEngine::Engine::instance()->addObject(adidasBall);
  }

  {
    QEasy3DEngine::Object *object = new QEasy3DEngine::Object;
    object->setColor(Qt::green);
    object->setDefaultMotionState(QQuaternion(1, 0, 0, 0), QVector3D(6, 20, 0));
    object->setShape(QEasy3DEngine::Engine::instance()->getShape("box-a-3-b-3-c-3"));
    object->setMass(1);
    object->setFriction(0.7);
    object->setRestitution(0.1);
    object->setNeedCollision();
    object->finalizeRigidBody();

    QEasy3DEngine::Engine::instance()->addObject(object);
  }

//  {
//    Object *object = new Object;
//    object->setDefaultMotionState(QQuaternion(1, 0, 0, 0), QVector3D(-5, 0, 0));
//    object->setShape(Engine3D::instance()->getShape("gate"));
//    object->setMass(0);
//    object->finalizeRigidBody();

//    Engine3D::instance()->addObject(object);
//  }

  QEasy3DEngine::Engine::instance()->addCollisionListener(this);

  QEasy3DEngine::Engine::instance()->startSimulation();
}

MainWindow::~MainWindow()
{
  delete ui;

  QEasy3DEngine::Engine::drop();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
  switch (event->key())
    {
    case Qt::Key_W:
      adidasBall->applyForce(QVector3D(-100, 0, 0));
      break;
    case Qt::Key_S:
      adidasBall->applyForce(QVector3D(100, 0, 0));
      break;
    case Qt::Key_A:
      adidasBall->applyForce(QVector3D(0, 0, 100));
      break;
    case Qt::Key_D:
      adidasBall->applyForce(QVector3D(0, 0, -100));
      break;
    case Qt::Key_Space:
      adidasBall->applyForce(QVector3D(0, 100, 0));
      break;
    }
}

void MainWindow::collisionHappend(QEasy3DEngine::Object *a, QEasy3DEngine::Object *b)
{
  if (a == groundObject || b == groundObject)
    {
      return;
    }
  qDebug() << "Collision box vs something other than the ground!" << a << b;
}
