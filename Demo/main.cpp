#include <QApplication>

#include "mainwindow.h"

#include "QEasy3DEngine/engine.h"

int main(int argc, char *argv[])
{
  Q_INIT_RESOURCE(resources);

  QApplication a(argc, argv);

  QEasy3DEngine::Engine::initialize();

  MainWindow w;
  w.show();
  
  return a.exec();
}
