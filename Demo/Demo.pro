TARGET = Demo3D

QT += widgets

CONFIG += qt3d

## UWA (don't remove it)##
INCDIR = $$PREFIX/include
LIBDIR = $$PREFIX/lib
BINDIR = $$PREFIX/bin

LIBS += -L$$LIBDIR -lQEasy3DEngine
INCLUDEPATH += $$INCDIR

# Useful commands:
#
target.path = $$BINDIR
INSTALLS += target
#
headerinstall.files = $$PWD/models/*
headerinstall.path = $$BINDIR/models
INSTALLS += headerinstall
#
# LIBS += -lxy

HEADERS += \
    mainwindow.h

SOURCES += \
    mainwindow.cpp \
    main.cpp

FORMS += \
    mainwindow.ui

RESOURCES += \
    resources.qrc

unix {
  CONFIG += link_pkgconfig
  PKGCONFIG += bullet Qt53D
}

LIBS += -lConvexDecomposition -lBulletDynamics -lBulletCollision -lLinearMath
