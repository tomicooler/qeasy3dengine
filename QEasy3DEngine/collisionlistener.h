#ifndef QEASY3DENGINE_COLLISIONLISTENER_H
#define QEASY3DENGINE_COLLISIONLISTENER_H

#include "object.h"

namespace QEasy3DEngine
{


  class CollisionListener
  {
  public:
    CollisionListener()
    { }

    virtual ~CollisionListener()
    { }

    virtual void collisionHappend(Object *a, Object *b) = 0;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_COLLISIONLISTENER_H
