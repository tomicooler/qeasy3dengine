#include "object.h"

#include <math.h>

#define DEG2RAD(x) (x * M_PI / 180.)

using namespace QEasy3DEngine;

quint64 Object::id = 0;

Object::Object(QObject *parent)
  : QObject(parent),
    mass(0.),
    friction(0.),
    restitution(0.),
    deactivation(false),
    rigidBody(0),
    shape(0),
    motionState(0),
    textureType(COLOR),
    needCollisionInfo(false),
    notInDynamicsWorld(false)
{
  ++id;
  myId = id;
}

Object::~Object()
{
  delete rigidBody;
  delete motionState;
}

void
Object::setDefaultMotionState(const QQuaternion &defaultRotation, const QVector3D &defaultPosition)
{
  Q_ASSERT(motionState == 0);
  motionState = new btDefaultMotionState(
                  btTransform(btQuaternion(defaultRotation.x(), defaultRotation.y(), defaultRotation.z(), defaultRotation.scalar()),
                  btVector3(defaultPosition.x(), defaultPosition.y(), defaultPosition.z())));
  position = defaultPosition;
  orientation = defaultRotation;
}

void
Object::setShape(Shape *shape)
{
  Q_ASSERT(this->shape == 0);
  this->shape = shape;
}

void
Object::finalizeRigidBody()
{
  Q_ASSERT(this->rigidBody == 0);
  Q_ASSERT(this->shape != 0);
  Q_ASSERT(this->shape->shape != 0);
  Q_ASSERT(this->motionState != 0);

  btVector3 inertia(0, 0, 0);
  shape->shape->calculateLocalInertia(mass, inertia);

  btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(mass, motionState, shape->shape, inertia);

  rigidBodyCI.m_restitution = restitution;
  rigidBodyCI.m_friction = friction;

  rigidBody = new btRigidBody(rigidBodyCI);

  rigidBody->setUserPointer(this);

  if (deactivation)
    {
      rigidBody->setActivationState(DISABLE_DEACTIVATION);
    }
}

void
Object::setMass(btScalar mass)
{
  Q_ASSERT(rigidBody == 0);
  this->mass = mass;
}

void
Object::setFriction(btScalar friction)
{
  Q_ASSERT(rigidBody == 0);
  this->friction = friction;
}

void
Object::setRestitution(btScalar restitution)
{
  Q_ASSERT(rigidBody == 0);
  this->restitution = restitution;
}

void
Object::disableDeactivation()
{
  Q_ASSERT(rigidBody == 0);
  deactivation = true;
}

void
Object::dontAddToDynmicsWorld()
{
  notInDynamicsWorld = true;
}

const QVector3D &
Object::getPosition() const
{
  return position;
}

const QQuaternion &
Object::getOrientation() const
{
  return orientation;
}

void
Object::setPosOriForObjectsNotInDynmicsWorld(const QVector3D &position, const QQuaternion &rotation)
{
  this->position = position;
  this->orientation = rotation;
}

void
Object::setTexture2D(const QString &texture)
{
  textureType = TEXTURE2D;
  texture2D.setImage(QImage(texture));
}

void
Object::setTexture2D(const QImage &image)
{
  textureType = TEXTURE2D;
  texture2D.setImage(image);
}

void
Object::setTextureCube(QGLTextureCube::Face face, const QString &texture)
{
  textureType = TEXTURECUBE;
  textureCube.setImage(face, QImage(texture));
}

void
Object::setTextureCube(QGLTextureCube::Face face, const QImage &image)
{
  textureType = TEXTURECUBE;
  textureCube.setImage(face, image);
}

void
Object::setColor(const QColor &color)
{
  textureType = COLOR;
  this->color = color;
}

void
Object::setNeedCollision()
{
  needCollisionInfo = true;
}

quint64
Object::getId() const
{
  return myId;
}

btRigidBody *
Object::getRigidBody()
{
  return rigidBody;
}

Shape *
Object::getShape()
{
  return shape;
}

void
Object::paintMe(QGLPainter *painter)
{
  painter->modelViewMatrix().push();

  if (textureType == TEXTURE2D)
    {
      painter->setStandardEffect(QGL::LitDecalTexture2D);
      texture2D.bind();
    }
  else if (textureType == TEXTURECUBE)
    {
      painter->setStandardEffect(QGL::LitDecalTexture2D);
      textureCube.bind();
    }
  else
    {
      painter->setStandardEffect(QGL::LitMaterial);
      painter->setFaceColor(QGL::AllFaces, color);
    }

  painter->modelViewMatrix().translate(position);
  painter->modelViewMatrix().rotate(orientation);

  shape->paintMe(painter);

  painter->modelViewMatrix().pop();
}

void
Object::applyForce(const QVector3D &force, const QVector3D &pos)
{
  Q_ASSERT(rigidBody != 0);
  rigidBody->applyForce(btVector3(force.x(), force.y(), force.z()), btVector3(pos.x(), pos.y(), pos.z()));
}

void
Object::applyRotation(const QVector3D &rotation)
{
  Q_ASSERT(rigidBody != 0);

  btTransform tr;
  rigidBody->getMotionState()->getWorldTransform(tr);

  btQuaternion quat;
  quat.setEuler(DEG2RAD(rotation.x()), DEG2RAD(rotation.y()), DEG2RAD(rotation.z()));
  tr.setRotation(quat);

  rigidBody->setCenterOfMassTransform(tr);
}

void
Object::updateMotionState()
{
  btTransform trans;
  rigidBody->getMotionState()->getWorldTransform(trans);

  position.setX(trans.getOrigin().getX());
  position.setY(trans.getOrigin().getY());
  position.setZ(trans.getOrigin().getZ());

  orientation.setScalar(trans.getRotation().getW());
  orientation.setVector(trans.getRotation().getX(),
                        trans.getRotation().getY(),
                        trans.getRotation().getZ());
}
