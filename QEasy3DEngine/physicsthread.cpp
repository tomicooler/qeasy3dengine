#include "physicsthread.h"

#include <QQuaternion>

#include "engine.h"

using namespace QEasy3DEngine;

PhysicsThread::PhysicsThread(QObject *parent) :
  QThread(parent),
  broadphase(new btDbvtBroadphase),
  collisionConfiguration(new btDefaultCollisionConfiguration),
  dispatcher(new btCollisionDispatcher(collisionConfiguration)),
  solver(new btSequentialImpulseConstraintSolver),
  dynamicsWorld(new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration)),
  running(false)
{
  // Gravity is Earth like
  dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

PhysicsThread::~PhysicsThread()
{
  delete dynamicsWorld;
  delete solver;
  delete collisionConfiguration;
  delete dispatcher;
  delete broadphase;
}

void
PhysicsThread::addObject(Object *object)
{
  QMutexLocker lock(&mutex);
  addQueue.enqueue(object);
}

void
PhysicsThread::pauseObject(Object *object)
{
  QMutexLocker lock(&mutex);
  pauseQueue.enqueue(object);
}

void
PhysicsThread::deleteObject(Object *object)
{
  QMutexLocker lock(&mutex);
  deleteQueue.enqueue(object);
}

void
PhysicsThread::addCollisionListener(CollisionListener *listener)
{
  listeners.append(listener);
}

void
PhysicsThread::removeCollisionListener(CollisionListener *listener)
{
  listeners.removeOne(listener);
}

void
PhysicsThread::stop()
{
  running = false;
}

void
PhysicsThread::collisionChecking()
{
  int numManifolds = dynamicsWorld->getDispatcher()->getNumManifolds();
  for (int i = 0; i < numManifolds; i++)
    {
      btPersistentManifold *contactManifold =
          dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
      const btCollisionObject *obA = static_cast< const btCollisionObject*
                               >(contactManifold->getBody0());
      const btCollisionObject *obB = static_cast< const btCollisionObject*
                               >(contactManifold->getBody1());

      Object *oA = (Object*)(obA->getUserPointer());
      Object *oB = (Object*)(obB->getUserPointer());

      if (oA && oB && (oA->needCollisionInfo || oB->needCollisionInfo))
        {
          int numContacts = contactManifold->getNumContacts();
          for (int j = 0; j < numContacts; j++)
            {
              btManifoldPoint& pt = contactManifold->getContactPoint(j);
              if (pt.getDistance() <= 0.001f)
                {
                  notifyListeners((Object*)obA->getUserPointer(), (Object*)obB->getUserPointer());
                }
            }
        }
    }
}

void
PhysicsThread::notifyListeners(Object *a, Object *b)
{
  foreach (CollisionListener *listener, listeners)
    {
      listener->collisionHappend(a, b);
    }
}

void
PhysicsThread::run()
{
  running = true;
  while (running)
    {
      {
        QMutexLocker lock(&mutex);

        while (!addQueue.isEmpty())
          {
            dynamicsWorld->addRigidBody(addQueue.dequeue()->rigidBody);
          }

        while (!pauseQueue.isEmpty())
          {
            dynamicsWorld->removeRigidBody(pauseQueue.dequeue()->rigidBody);
          }

        while (!deleteQueue.isEmpty())
          {
            Object *object = deleteQueue.dequeue();
            dynamicsWorld->removeRigidBody(object->rigidBody);
            object->deleteLater();
          }

      }

      dynamicsWorld->stepSimulation(1/60.f, 10);

      QList< Object* > objects = Engine::instance()->getObjects();

      foreach (Object *object, objects)
        {
          if (!object->notInDynamicsWorld)
            {
              object->updateMotionState();
            }
        }

      collisionChecking();

      usleep(10000);
    }
}
