#ifndef QEASY3DENGINE_SHAPE_H
#define QEASY3DENGINE_SHAPE_H

#include <QObject>
#include <QString>

#include <Qt3D/qglpainter.h>
#include <Qt3D/qglscenenode.h>

#include <btBulletCollisionCommon.h>

namespace QEasy3DEngine
{

  class PhysicsThread;
  class Object;

  class Shape
  {
  public:
    Shape(const QString &name);

    virtual ~Shape();

    const QString& getName() const
    {
      return name;
    }

    virtual void paintMe(QGLPainter */*painter*/) = 0;

    friend class PhysicsThread;
    friend class Object;

  protected:

    btCollisionShape *shape;

    QString name;

    static QGLSceneNode *boxNode;
    static QGLSceneNode *sphereNode;
    static qint64 shapeCount;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_SHAPE_H
