#include "engine.h"

#include <QMutex>
#include <QMutexLocker>

using namespace QEasy3DEngine;

static QMutex mutex;

Engine *Engine::container = 0;

void
Engine::initialize()
{
  Q_ASSERT(container == 0);
  QMutexLocker lock(&mutex);
  container = new Engine;
}


void
Engine::drop()
{
  Q_ASSERT(container != 0);
  container->deleteObjects();
  container->deleteShapes();
  container->physicsThread->stop();
  container->physicsThread->wait();
  QMutexLocker lock(&mutex);
  delete container;
  container = 0;
}

Engine*
Engine::instance()
{
  Q_ASSERT(container != 0);
  QMutexLocker lock(&mutex);
  return container;
}

void
Engine::startSimulation()
{
  QMutexLocker lock(&mutex);
  physicsThread->start();
}

Object*
Engine::getObject(quint64 id)
{
  QMap< quint64, Object* >::iterator it = objects.find(id);
  Q_ASSERT(it != objects.end());

  return it.value();
}

Shape*
Engine::getShape(const QString &name)
{
  QMap< QString, Shape* >::iterator it = shapes.find(name);
  Q_ASSERT(it != shapes.end());

  return it.value();
}

void
Engine::addObject(Object *object)
{
  Q_ASSERT(object != 0);
  objects[object->id] = object;

  if (!object->notInDynamicsWorld)
    {
      physicsThread->addObject(object);
    }
}

void
Engine::addShape(Shape *shape)
{
  Q_ASSERT(shape != 0);
  shapes[shape->getName()] = shape;
}

void
Engine::pauseObject(Object *object)
{
  physicsThread->pauseObject(object);
}

void
Engine::pauseObject(quint64 id)
{
  QMap< quint64, Object* >::iterator it = objects.find(id);
  if (it != objects.end())
    {
      physicsThread->pauseObject(it.value());
    }
}

void
Engine::deleteObject(Object *object)
{
  objects.remove(object->getId());
  physicsThread->deleteObject(object);
}

void
Engine::deleteObject(quint64 id)
{
  QMap< quint64, Object* >::iterator it = objects.find(id);
  if (it != objects.end())
    {
      Object *object = it.value();
      objects.erase(it);
      physicsThread->deleteObject(object);
    }
}

void
Engine::removeShape(Shape *shape)
{
  shapes.remove(shape->getName());
}

void
Engine::removeShape(const QString &name)
{
  shapes.remove(name);
}

QList< Object * >
Engine::getObjects() const
{
  return objects.values();
}

QList< Shape * >
Engine::getShapes() const
{
  return shapes.values();
}

void
Engine::addCollisionListener(CollisionListener *listener)
{
  physicsThread->addCollisionListener(listener);
}

void
Engine::removeCollisionListener(CollisionListener *listener)
{
  physicsThread->removeCollisionListener(listener);
}

Engine::Engine()
  : physicsThread(new PhysicsThread)
{
}

Engine::Engine(const Engine &other)
{
  Q_UNUSED(other);
}

Engine::~Engine()
{
  delete physicsThread;
}

void
Engine::deleteObjects()
{
  foreach (Object *obj, objects.values())
    {
      physicsThread->deleteObject(obj);
    }

  objects.clear();
}

void
Engine::deleteShapes()
{
  QList< Shape* > shps = shapes.values();
  shapes.clear();
  while (shps.count() > 0)
    {
      delete shps.takeFirst();
    }
}
