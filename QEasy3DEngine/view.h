#ifndef QEASY3DENGINE_VIEW_H
#define QEASY3DENGINE_VIEW_H

#include <QTimer>

#include <Qt3D/QGLView>

class QGLSceneNode;

namespace QEasy3DEngine
{

  class View : public QGLView
  {
    Q_OBJECT

  public:
    View(int updateFps = 50, QWindow *parent = 0);
    virtual ~View();

    virtual void paintGL(QGLPainter *painter);

  private:
    QTimer fpsTimer;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_VIEW_H
