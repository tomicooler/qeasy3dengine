#include "shape.h"

#include <Qt3D/qglbuilder.h>
#include <Qt3D/qglcube.h>
#include <Qt3D/qglsphere.h>

using namespace QEasy3DEngine;

QGLSceneNode *Shape::boxNode = 0;
QGLSceneNode *Shape::sphereNode = 0;
qint64 Shape::shapeCount = 0;

Shape::Shape(const QString &name)
  : shape(0),
    name(name)
{
  if (shapeCount == 0)
    {
      if (!boxNode)
        {
          QGLBuilder builder;
          builder << QGL::Faceted;
          builder << QGLCube();
          boxNode = builder.finalizedSceneNode();
        }

      if (!sphereNode)
        {
          QGLBuilder builder;
          builder << QGL::Faceted;
          builder << QGLSphere();
          sphereNode = builder.finalizedSceneNode();
        }
    }

  ++shapeCount;
}

Shape::~Shape()
{
  delete shape;
  --shapeCount;
  if (shapeCount == 0)
    {
      delete boxNode;
      delete sphereNode;
      boxNode = 0;
      sphereNode = 0;
    }
}
