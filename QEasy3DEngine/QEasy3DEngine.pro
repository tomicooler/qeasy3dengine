TARGET = QEasy3DEngine
TEMPLATE = lib

QT += widgets

CONFIG += dll
CONFIG -= staticlib

HEADERS += \
    physicsthread.h \
    object.h \
    shape.h \
    boxshape.h \
    sphereshape.h \
    abstractshape.h \
    staticplaneshape.h \
    collisionlistener.h \
    engine.h \
    view.h \
    windowwidget.h

SOURCES += \
    physicsthread.cpp \
    object.cpp \
    shape.cpp \
    engine.cpp \
    view.cpp \
    windowwidget.cpp

CONFIG += qt3d

## UWA (don't remove it)##
INCDIR = $$PREFIX/include
LIBDIR = $$PREFIX/lib
BINDIR = $$PREFIX/bin

LIBS += -L$$LIBDIR
INCLUDEPATH += $$INCDIR

# Useful commands:
#
target.path = $$LIBDIR
INSTALLS += target

headerinstall.files = $$HEADERS
headerinstall.path = $$INCDIR/QEasy3DEngine
INSTALLS += headerinstall
#
# LIBS += -lxy

unix {
  CONFIG += link_pkgconfig
  PKGCONFIG += bullet Qt53D
}

windows {
  message("Windows build")
  QMAKE_LFLAGS = -enable-stdcall-fixup -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc
}

LIBS += -lConvexDecomposition -lBulletDynamics -lBulletCollision -lLinearMath
