#ifndef QEASY3DENGINE_OBJECT_H
#define QEASY3DENGINE_OBJECT_H

#include <QVector3D>
#include <QQuaternion>
#include <QColor>

#include <Qt3D/qglpainter.h>
#include <Qt3D/qgltexture2d.h>
#include <Qt3D/qgltexturecube.h>

#include <btBulletDynamicsCommon.h>

#include "shape.h"

namespace QEasy3DEngine
{


  class PhsysicsThread;
  class View;
  class Engine;

  class Object : public QObject
  {
    Q_OBJECT

  public:
    Object(QObject *parent = 0);
    virtual ~Object();

    // function before finalizy rigid body
    void setDefaultMotionState(const QQuaternion &defaultRotation = QQuaternion(1, 0, 0, 0), const QVector3D &defaultPosition = QVector3D(0, 0, 0));
    void setShape(Shape *shape);
    void setMass(btScalar mass);
    void setFriction(btScalar friction);
    void setRestitution(btScalar restitution);
    void setTexture2D(const QImage &image);
    void setTexture2D(const QString &texture);
    void setTextureCube(QGLTextureCube::Face face, const QString &texture);
    void setTextureCube(QGLTextureCube::Face face, const QImage &image);
    void setColor(const QColor &color);
    void setNeedCollision();
    void disableDeactivation();
    void dontAddToDynmicsWorld();
    void finalizeRigidBody();

    // getters
    btRigidBody* getRigidBody();
    Shape* getShape();

    const QVector3D& getPosition() const;
    const QQuaternion& getOrientation() const;

    // functions after finalize rigid body
    void setPosOriForObjectsNotInDynmicsWorld(const QVector3D &position, const QQuaternion &rotation);
    void applyForce(const QVector3D &force, const QVector3D &pos = QVector3D(0, 0, 0));
    void applyRotation(const QVector3D &rotation);

    quint64 getId() const;

    friend class PhysicsThread;
    friend class View;
    friend class Engine;

  protected:
    void paintMe(QGLPainter *painter);

    void updateMotionState();

    enum TextureType
    {
      COLOR = 0,
      TEXTURE2D,
      TEXTURECUBE
    };

    static quint64 id;
    quint64 myId;

    btScalar mass;
    btScalar friction;
    btScalar restitution;

    bool deactivation;

    btRigidBody *rigidBody;
    Shape *shape;
    btDefaultMotionState *motionState;

    QVector3D position;
    QQuaternion orientation;

    TextureType textureType;
    QGLTexture2D texture2D;
    QGLTextureCube textureCube;

    QColor color;

    bool needCollisionInfo;

    bool notInDynamicsWorld;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_OBJECT_H
