#ifndef QEASY3DENGINE_ABSTRACTSHAPE_H
#define QEASY3DENGINE_ABSTRACTSHAPE_H

/// TODO: this is totally a mess should be reimplemented, the scales are bad, etc...

#include <Qt3D/qglabstractscene.h>

#include <ConvexDecomposition/cd_wavefront.h>
#include <BulletCollision/CollisionShapes/btShapeHull.h>

#include "shape.h"

namespace QEasy3DEngine
{

  class AbstractShape : public Shape
  {
  public:
    AbstractShape(const QString &name, const QString &scene)
      : Shape(name)
    {
      abstractNode = QGLAbstractScene::loadScene(scene);

      Q_ASSERT(abstractNode != 0);

      ConvexDecomposition::WavefrontObj wo;

      unsigned int a = wo.loadObj(scene.toUtf8().data());

      if (a > 0) // object was succesfully loaded
        {
          btTriangleMesh* trimesh = new btTriangleMesh();

          btVector3 localScaling(6.f,6.f,6.f);

          int i;
          for (i=0;i<wo.mTriCount;i++)
            {
              int index0 = wo.mIndices[i*3];
              int index1 = wo.mIndices[i*3+1];
              int index2 = wo.mIndices[i*3+2];

              btVector3 vertex0(wo.mVertices[index0*3], wo.mVertices[index0*3+1],wo.mVertices[index0*3+2]);
              btVector3 vertex1(wo.mVertices[index1*3], wo.mVertices[index1*3+1],wo.mVertices[index1*3+2]);
              btVector3 vertex2(wo.mVertices[index2*3], wo.mVertices[index2*3+1],wo.mVertices[index2*3+2]);

              vertex0 *= localScaling;
              vertex1 *= localScaling;
              vertex2 *= localScaling;

              trimesh->addTriangle(vertex0,vertex1,vertex2);
            }

          btConvexShape* tmpConvexShape = new btConvexTriangleMeshShape(trimesh);

          // create a hull approximation
          btShapeHull* hull = new btShapeHull(tmpConvexShape);
          btScalar margin = tmpConvexShape->getMargin();
          hull->buildHull(margin);
          tmpConvexShape->setUserPointer(hull);

          btConvexHullShape* convexShape = new btConvexHullShape();
          for (i=0;i<hull->numVertices();i++)
            {
              convexShape->addPoint(hull->getVertexPointer()[i]);
            }

          delete tmpConvexShape;
          delete hull;

          shape = convexShape;
        }

    }

    virtual ~AbstractShape()
    { }

    void paintMe(QGLPainter *painter)
    {
      painter->modelViewMatrix().scale(2);
      abstractNode->mainNode()->draw(painter);
      //abstractNode->mainNode()->backMaterial()->bind(painter);
      //abstractNode->mainNode()->material()->bind(painter);
      if (abstractNode->mainNode()->material())
        {
          abstractNode->mainNode()->material()->bind(painter);
        }
    }

  private:

    QGLAbstractScene *abstractNode;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_ABSTRACTSHAPE_H
