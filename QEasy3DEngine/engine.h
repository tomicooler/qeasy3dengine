#ifndef QEASY3DENGINE_ENGINE_H
#define QEASY3DENGINE_ENGINE_H

#include <QString>
#include <QMap>

#include "physicsthread.h"

#include "object.h"
#include "shape.h"
#include "boxshape.h"
#include "sphereshape.h"
#include "abstractshape.h"
#include "staticplaneshape.h"

namespace QEasy3DEngine
{


  class Engine
  {

  public:
    static void initialize();
    static void drop();

    static Engine* instance();

    void startSimulation();

    Object* getObject(quint64 id);
    Shape* getShape(const QString &name);

    void addObject(Object *object);
    void addShape(Shape *shape);

    void pauseObject(Object *object);
    void pauseObject(quint64 id);

    void deleteObject(Object *object);
    void deleteObject(quint64 id);

    void removeShape(Shape *shape);
    void removeShape(const QString &name);

    QList< Object* > getObjects() const;
    QList< Shape* > getShapes() const;

    void addCollisionListener(CollisionListener *listener);
    void removeCollisionListener(CollisionListener *listener);

  private:
    Engine();
    Engine(const Engine &other);
    virtual ~Engine();

    void deleteObjects();
    void deleteShapes();

    static Engine *container;

    QMap< quint64, Object* > objects;
    QMap< QString, Shape* > shapes;

    PhysicsThread *physicsThread;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_ENGINE_H
