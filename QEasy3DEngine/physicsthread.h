#ifndef QEASY3DENGINE_PHYSICSTHREAD_H
#define QEASY3DENGINE_PHYSICSTHREAD_H

#include <QCoreApplication>

#include <QList>
#include <QThread>
#include <QVector3D>

#include <QQueue>
#include <QMutexLocker>
#include <QMutex>

#include <btBulletDynamicsCommon.h>

#include "collisionlistener.h"

namespace QEasy3DEngine
{

  class PhysicsThread : public QThread
  {
    Q_OBJECT

  public:

    PhysicsThread(QObject *parent = 0);

    virtual ~PhysicsThread();

    void addObject(Object *object);
    void pauseObject(Object *object);
    void deleteObject(Object *object);

    void addCollisionListener(CollisionListener *listener);
    void removeCollisionListener(CollisionListener *listener);

    void stop();

  protected:

    void run();

  private:

    void collisionChecking();

    void notifyListeners(Object *a, Object *b);

    btBroadphaseInterface* broadphase;
    btDefaultCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* dispatcher;
    btSequentialImpulseConstraintSolver* solver;
    btDiscreteDynamicsWorld* dynamicsWorld;

    bool running;

    QList< CollisionListener* > listeners;

    QQueue< Object* > addQueue;
    QQueue< Object* > pauseQueue;
    QQueue< Object* > deleteQueue;


    QMutex mutex;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_PHYSICSTHREAD_H
