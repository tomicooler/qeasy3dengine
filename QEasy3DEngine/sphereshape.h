#ifndef QEASY3DENGINE_SPHERESHAPE_H
#define QEASY3DENGINE_SPHERESHAPE_H

#include "shape.h"

namespace QEasy3DEngine
{

  class SphereShape : public Shape
  {
  public:
    SphereShape(const QString &name, btScalar radius)
      : Shape(name),
        radius(radius)
    {
      shape = new btSphereShape(radius / 2.);
    }

    virtual ~SphereShape()
    { }

    void paintMe(QGLPainter *painter)
    {
      painter->modelViewMatrix().scale(radius, radius, radius);
      sphereNode->draw(painter);
    }

  private:

    btScalar radius;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_SPHERESHAPE_H
