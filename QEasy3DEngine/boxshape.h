#ifndef QEASY3DENGINE_BOXSHAPE_H
#define QEASY3DENGINE_BOXSHAPE_H

#include "shape.h"

namespace QEasy3DEngine
{


  class BoxShape : public Shape
  {
  public:
    BoxShape(const QString &name, btScalar a, btScalar b, btScalar c)
      : Shape(name),
        a(a),
        b(b),
        c(c)
    {
      shape = new btBoxShape(btVector3(a / 2., b / 2., c / 2.));
    }

    virtual ~BoxShape()
    { }

    void paintMe(QGLPainter *painter)
    {
      painter->modelViewMatrix().scale(a, b, c);
      boxNode->draw(painter);
    }

  private:

    btScalar a;
    btScalar b;
    btScalar c;

  };


} // QEasy3DEngine

#endif // QEASY3DENGINE_BOXSHAPE_H
