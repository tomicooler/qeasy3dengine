#include "view.h"

#include <Qt3D/qglbuilder.h>
#include <Qt3D/qglcube.h>
#include <Qt3D/qglsphere.h>

#include "engine.h"

using namespace QEasy3DEngine;

View::View(int updateFps, QWindow *parent)
  : QGLView(parent)
{
  connect(&fpsTimer, SIGNAL(timeout()), this, SLOT(update()));
  fpsTimer.setInterval(1000 / updateFps);
  fpsTimer.start();
}

View::~View()
{
  fpsTimer.stop();
}

void
View::paintGL(QGLPainter *painter)
{
  QList< Object* > objects = Engine::instance()->getObjects();

  foreach (Object *object, objects)
    {
      object->paintMe(painter);
    }
}
