#ifndef QEASY3DENGINE_STATICPLANESHAPE_H
#define QEASY3DENGINE_STATICPLANESHAPE_H

#include "shape.h"

namespace QEasy3DEngine
{

  class StaticPlaneShape : public Shape
  {
  public:
    StaticPlaneShape(const QString &name, btScalar a, btScalar b, btScalar c)
      : Shape(name),
        a(a),
        b(b),
        c(c)
    {
      shape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
    }

    virtual ~StaticPlaneShape()
    { }

    void paintMe(QGLPainter *painter)
    {
      painter->modelViewMatrix().scale(a, b, c);
      boxNode->draw(painter);
    }

  private:
    btScalar a;
    btScalar b;
    btScalar c;

  };

} // QEasy3DEngine

#endif // QEASY3DENGINE_STATICPLANESHAPE_H
